<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PertanyaanController extends Controller
{
    public function index()
    {
        // mengambil data dari table pegawai
        $pertanyaan = DB::table('pertanyaan')->get();

        // mengirim data pertanyaan ke view index
        return view('pertanyaan', ['pertanyaan' => $pertanyaan]);
    }
}
