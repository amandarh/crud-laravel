<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <title>Social Media</title>
  </head>
  <body>
    @include("navbar")
    <div class="row header-container justify-content-center">
        <div class="header">
            <h1>Social Media</h1>
        </div>
    </div>
    
    <table border="1">
		<tr>
			<th>Judul</th>
			<th>isi</th>
			<th>jawaban</th>
			
		</tr>
		@foreach($pertanyaan as $p)
		<tr>
			<td>{{ $p->judul }}</td>
			<td>{{ $p->isi }}</td>
			<td>{{ $p->jawaban }}</td>
			<td>
				<a href="/pertanyaan/edit/{{ $p->pertanyaan_id }}">Edit</a>
				|
				<a href="/pertanyaan/hapus/{{ $p->pertanyaan_id }}">Hapus</a>
			</td>
		</tr>
		@endforeach
	</table>
    
    <footer></footer>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
  </body>
</html>