<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/pertanyaan', "PertanyaanController@index");
Route::get('/pertanyaan/edit/{id}', "PertanyaanController@edit");
Route::get('/pertanyaan/show/{id}', "PertanyaanController@show");
Route::get('/pertanyaan/create', "PertanyaanController@create");
Route::post('/pertanyaan/store', "PertanyaanController@store");
Route::post('/pertanyaan/update/{id}', "PertanyaanController@update");
Route::get('/pertanyaan/delete/{id}', "PertanyaanController@destroy");
